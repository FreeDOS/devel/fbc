# FreeBASIC

see

- [https://sourceforge.net/projects/fbc/](https://sourceforge.net/projects/fbc/)
- [https://github.com/freebasic/fbc](https://github.com/freebasic/fbc)

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## FBC.LSM

<table>
<tr><td>title</td><td>FreeBASIC</td></tr>
<tr><td>version</td><td>1.10.1</td></tr>
<tr><td>entered&nbsp;date</td><td>2023-12-25</td></tr>
<tr><td>description</td><td>FreeBASIC, a 32-bit BASIC compiler</td></tr>
<tr><td>keywords</td><td>basic, compiler</td></tr>
<tr><td>author</td><td>FreeBASIC Development team</td></tr>
<tr><td>maintained&nbsp;by</td><td>Daniel Verkamp</td></tr>
<tr><td>primary&nbsp;site</td><td>http://sourceforge.net/projects/fbc/</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://github.com/freebasic/fbc</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.freebasic.net</td></tr>
<tr><td>platforms</td><td>DOS, Windows, Linux</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
